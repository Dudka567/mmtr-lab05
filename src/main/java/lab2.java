import java.util.*;

public class lab2 {
    public static void main(String[] args)  {
        HashMap<String,String> map = new HashMap<String,String>();
        inMap("a","10",map);
        inMap("a","20",map);
        inMap("b","15",map);
        inMap("с","10",map);
        System.out.println(map.toString());
        reverseKV(map);
    }
    public static void inMap(String key, String value, Map map)
    {
        if(map.get(key)!=null)
            map.compute(key,(k, v) -> v + ","+value);
        else
            map.put(key,value);
    }
    public static void reverseKV(Map map)
    {
        HashMap<String, String> map1 = new HashMap<String, String>();
        for(Object item:map.keySet())
        {
            String[] values = map.get(item).toString().split(",");
            for(String value:values)
            {
                inMap(value,item.toString(),map1);
            }
        }
        map.clear();
        map = map1;
        System.out.println(map.toString());
    }
}


